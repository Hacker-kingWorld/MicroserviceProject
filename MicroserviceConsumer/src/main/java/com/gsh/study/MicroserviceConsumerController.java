package com.gsh.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author GSh
 * @packageName:com.gsh.study
 * @className:MicroserviceProviderController
 * @description
 * @create 2021-11-08 14:49
 */
@SpringBootApplication
@EnableDiscoveryClient
public class MicroserviceConsumerController {
    public static void main(String[] args) {
        SpringApplication.run(MicroserviceConsumerController.class,args);
    }
}
