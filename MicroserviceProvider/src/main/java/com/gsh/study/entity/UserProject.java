package com.gsh.study.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

/**
 * @author GSh
 * @packageName:com.gsh.study.entity
 * @className:UserProject
 * @description
 * @create 2021-11-08 15:46
 */
@TableName("t_project")
public class UserProject {
    /** 主键 */
    @TableId(type = IdType.AUTO)
    private Integer id ;
    /** 项目名 */
    private String pname ;
    /** 创建者 */
    private String author ;
    /** 创建信息 */
    private String pinfo ;
    /** 更新时间 */
    private Date pkname ;
    /** 数据库地址 */
    private String dburl ;
    /** 数据库端口号 */
    private String dbport ;
    /** 数据库名称 */
    private String dbname ;
    /** 数据库用户 */
    private String dbuser ;
    /** 数据库密码 */
    private String dbpass ;
    /** 创建时间 */
    private Date ctime ;

    /** 主键 */
    public Integer getId(){
        return this.id;
    }
    /** 主键 */
    public void setId(Integer id){
        this.id = id;
    }
    /** 项目名 */
    public String getPname(){
        return this.pname;
    }
    /** 项目名 */
    public void setPname(String pname){
        this.pname = pname;
    }
    /** 创建者 */
    public String getAuthor(){
        return this.author;
    }
    /** 创建者 */
    public void setAuthor(String author){
        this.author = author;
    }
    /** 创建信息 */
    public String getPinfo(){
        return this.pinfo;
    }
    /** 创建信息 */
    public void setPinfo(String pinfo){
        this.pinfo = pinfo;
    }
    /** 更新时间 */
    public Date getPkname(){
        return this.pkname;
    }
    /** 更新时间 */
    public void setPkname(Date pkname){
        this.pkname = pkname;
    }
    /** 数据库地址 */
    public String getDburl(){
        return this.dburl;
    }
    /** 数据库地址 */
    public void setDburl(String dburl){
        this.dburl = dburl;
    }
    /** 数据库端口号 */
    public String getDbport(){
        return this.dbport;
    }
    /** 数据库端口号 */
    public void setDbport(String dbport){
        this.dbport = dbport;
    }
    /** 数据库名称 */
    public String getDbname(){
        return this.dbname;
    }
    /** 数据库名称 */
    public void setDbname(String dbname){
        this.dbname = dbname;
    }
    /** 数据库用户 */
    public String getDbuser(){
        return this.dbuser;
    }
    /** 数据库用户 */
    public void setDbuser(String dbuser){
        this.dbuser = dbuser;
    }
    /** 数据库密码 */
    public String getDbpass(){
        return this.dbpass;
    }
    /** 数据库密码 */
    public void setDbpass(String dbpass){
        this.dbpass = dbpass;
    }
    /** 创建时间 */
    public Date getCtime(){
        return this.ctime;
    }
    /** 创建时间 */
    public void setCtime(Date ctime){
        this.ctime = ctime;
    }
}
