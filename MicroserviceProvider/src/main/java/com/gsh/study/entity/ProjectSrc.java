package com.gsh.study.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

/**
 * @author GSh
 * @packageName:com.gsh.study.entity
 * @className:ProjectSrc
 * @description
 * @create 2021-11-08 15:49
 */
@TableName("t_project")
public class ProjectSrc {
    /** 主键 */
    @TableId(type = IdType.AUTO)
    private Integer id ;
    /** 用户项目表的主键 */
    private Integer pid ;
    /** 包名 */
    private Date bname ;
    /** 实体名 */
    private String objname ;
    /** 数据库路径 */
    private String srcurl ;
    /** 创建时间 */
    private Date ctime ;
    /** 下载截止时间 */
    private Date etime ;
    /** 主键 */
    public Integer getId(){
        return this.id;
    }
    /** 主键 */
    public void setId(Integer id){
        this.id = id;
    }
    /** 用户项目表的主键 */
    public Integer getPid(){
        return this.pid;
    }
    /** 用户项目表的主键 */
    public void setPid(Integer pid){
        this.pid = pid;
    }
    /** 包名 */
    public Date getBname(){
        return this.bname;
    }
    /** 包名 */
    public void setBname(Date bname){
        this.bname = bname;
    }
    /** 实体名 */
    public String getObjname(){
        return this.objname;
    }
    /** 实体名 */
    public void setObjname(String objname){
        this.objname = objname;
    }
    /** 数据库路径 */
    public String getSrcurl(){
        return this.srcurl;
    }
    /** 数据库路径 */
    public void setSrcurl(String srcurl){
        this.srcurl = srcurl;
    }
    /** 创建时间 */
    public Date getCtime(){
        return this.ctime;
    }
    /** 创建时间 */
    public void setCtime(Date ctime){
        this.ctime = ctime;
    }
    /** 下载截止时间 */
    public Date getEtime(){
        return this.etime;
    }
    /** 下载截止时间 */
    public void setEtime(Date etime){
        this.etime = etime;
    }
}
