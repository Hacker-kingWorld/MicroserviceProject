package com.gsh.study.core;

import java.io.File;

/**
 * @author GSh
 * @packageName:com.gsh.study.core
 * @className:FileUtils
 * @description
 * @create 2021-11-08 17:36
 */
public class FileUtils {
    public static String baseDir = "codegen";
    public static String[] pkNames;

    static {
        pkNames = new String[]{
                "entity",
                "dao",
                "service/impl",
                "service/intf",
                "mapper",
                "controller",
                "vo"
        };
    }

    public static String parsePageNameDir(String basePk) {
        File file = new File(baseDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        String r = basePk.replace("[.]", "/");
        File pkFile = new File(file, r);
        if (!pkFile.exists()) {
            pkFile.mkdirs();
        }
        for (String s : pkNames
        ) {
            File child = new File(pkFile,s);
            if (!child.exists()){
                child.mkdirs();
            }
        }

        return pkFile.getAbsolutePath();
    }
}
